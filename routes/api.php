<?php


use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SettingTypeController;
use Illuminate\Support\Facades\Route;


Route::get('/', [HomeController::class, 'index']);
Route::controller(ServiceController::class)->group(function () {
    Route::group(['prefix' => 'services'], function () {
        Route::get('/', 'index');
        Route::post('/', 'store');
        Route::get('/{id}', 'show');
        Route::post('/update', 'update');
        Route::post('/delete', 'destroy');
    });
});
// Settings
Route::controller(SettingController::class)->group(function () {
    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', 'index');
        Route::post('/', 'store');
        Route::get('/{id}', 'show');
        Route::post('/update', 'update');
        Route::post('/delete', 'destroy');
    });
});
// Setting Types
Route::controller(SettingTypeController::class)->group(function () {
    Route::group(['prefix' => 'settingTypes'], function () {
        Route::get('/', 'index');
        Route::post('/', 'store');
        Route::get('/{id}', 'show');
        Route::post('/update', 'update');
        Route::post('/delete', 'destroy');
    });
});
// Contact
Route::controller(ContactController::class)->group(function () {
    Route::group(['prefix' => 'contacts'], function () {
            Route::get('/', 'index');
            Route::post('/', 'store');
            Route::get('/{id}', 'show');
            Route::post('/update', 'update');
            Route::post('/delete', 'delete');
    });
});
