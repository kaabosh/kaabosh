<?php

namespace App\Http\Controllers;

use App\Http\Interfaces\ContactInterface;
use App\Http\Requests\Contact\ContactRequest;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    private $contactInterfce;

    public function __construct(ContactInterface $contact)
    {
        $this->contactInterfce = $contact;
    }

    public function index()
    {
        return $this->contactInterfce->index();
    }

    public function store(ContactRequest $request)
    {
        return $this->contactInterfce->store($request);
    }

    public function show($id)
    {
        return $this->contactInterfce->show($id);
    }

    public function update(ContactRequest $request)
    {
        return $this->contactInterfce->update($request);
    }

    public function delete(ContactRequest $request)
    {
        return $this->contactInterfce->delete($request);
    }
}
