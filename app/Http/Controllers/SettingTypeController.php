<?php

namespace App\Http\Controllers;

use App\Http\Interfaces\SettingTypeInterface;
use App\Http\Requests\SettingType\AddSettingTypeRequest;
use App\Http\Requests\SettingType\DeleteSettingTypeRequest;
use App\Http\Requests\SettingType\UpdateSettingTypeRequest;
use App\Http\Traits\apiResponse;

class SettingTypeController extends Controller
{
    use apiResponse;
    private $settingTypeInterface;

    public function __construct(SettingTypeInterface $settingType)
    {
        $this->settingTypeInterface = $settingType;
    }

    public function index()
    {
        return $this->settingTypeInterface->index();
    }
    public function store(AddSettingTypeRequest $request)
    {
        try {
            return $this->settingTypeInterface->store($request);
        }catch (\Exception $exception){
            return $this->apiResponse(400,$exception->getMessage());
        }
    }
    public function show($id)
    {
        return $this->settingTypeInterface->show($id);
    }
    public function update(UpdateSettingTypeRequest $request)
    {
        return $this->settingTypeInterface->update($request);
    }
    public function destroy(DeleteSettingTypeRequest $request)
    {
        return $this->settingTypeInterface->destroy($request);
    }
}
