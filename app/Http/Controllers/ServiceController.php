<?php

namespace App\Http\Controllers;

use App\Http\Interfaces\ServiceInterface;
use App\Http\Requests\Service\AddServiceRequest;
use App\Http\Requests\Service\DeleteServiceRequest;
use App\Http\Requests\Service\UpdateServiceRequest;
use App\Http\Traits\apiResponse;

class ServiceController extends Controller
{
    use apiResponse;
    private $serviceInterface;

    public function __construct(ServiceInterface $service)
    {
        $this->serviceInterface = $service;
    }

    public function index()
    {
        return $this->serviceInterface->index();
    }
    public function store(AddServiceRequest $request)
    {
        try {
            return $this->serviceInterface->store($request);
        }catch (\Exception $exception){
            return $this->apiResponse(400,$exception->getMessage());
        }
    }
    public function show($id)
    {
        return $this->serviceInterface->show($id);
    }
    public function update(UpdateServiceRequest $request)
    {
        return $this->serviceInterface->update($request);
    }
    public function destroy(DeleteServiceRequest $request)
    {
        return $this->serviceInterface->destroy($request);
    }
}
