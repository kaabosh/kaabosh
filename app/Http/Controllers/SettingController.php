<?php

namespace App\Http\Controllers;

use App\Http\Interfaces\SettingInterface;
use App\Http\Requests\Setting\AddSettingRequest;
use App\Http\Requests\Setting\DeleteSettingRequest;
use App\Http\Requests\Setting\UpdateSettingRequest;
use App\Http\Traits\apiResponse;

class SettingController extends Controller
{
    use apiResponse;
    private $settingInterface;

    public function __construct(SettingInterface $setting)
    {
        $this->settingInterface = $setting;
    }

    public function index()
    {
        return $this->settingInterface->index();
    }
    public function store(AddSettingRequest $request)
    {
        try {
            return $this->settingInterface->store($request);
        }catch (\Exception $exception){
            return $this->apiResponse(400,$exception->getMessage());
        }
    }
    public function show($id)
    {
        return $this->settingInterface->show($id);
    }
    public function update(UpdateSettingRequest $request)
    {
        return $this->settingInterface->update($request);
    }
    public function destroy(DeleteSettingRequest $request)
    {
        return $this->settingInterface->destroy($request);
    }
}
