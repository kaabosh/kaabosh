<?php


namespace App\Http\Traits;


trait SettingTrait
{
    private function get_all_settings()
    {
        return $this->settingsModel::with('type')->get();
    }
    private function store_setting($request)
    {
        return $this->settingsModel::create($request->validated());
    }
    private function get_setting($id)
    {
        return $this->settingsModel::find($id);
    }
    private function update_setting($request)
    {
        $setting=$this->settingsModel::with('type')->find($request->id);
        $setting->update($request->validated());
        return $setting;
    }
    private function destroy_setting($id){
        return $this->settingsModel::destroy($id);
    }
}
