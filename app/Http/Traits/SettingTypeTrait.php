<?php


namespace App\Http\Traits;


trait SettingTypeTrait
{
    private function get_all_settingTypes()
    {
        return $this->settingTypesModel::get();
    }
    private function store_settingType($request)
    {
        return $this->settingTypesModel::create($request->validated());
    }
    private function get_settingType($id)
    {
        return $this->settingTypesModel::find($id);
    }
    private function update_settingType($request)
    {
        $settingType=$this->settingTypesModel::find($request->id);
        $settingType->update($request->validated());
        return $settingType;
    }
    private function destroy_settingType($id){
        return $this->settingTypesModel::destroy($id);
    }
}
