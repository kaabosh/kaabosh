<?php


namespace App\Http\Traits;


trait ServiceTrait
{
    private function get_all_services()
    {
        return $this->servicesModel::get();
    }
    private function store_service($request)
    {
        return $this->servicesModel::create($request->validated());
    }
    private function get_service($id)
    {
        return $this->servicesModel::find($id);
    }
    private function update_service($request)
    {
        $service=$this->servicesModel::find($request->id);
        $service->update($request->validated());
        return $service;
    }
    private function destroy_service($id){
        return $this->servicesModel::destroy($id);
    }
}
