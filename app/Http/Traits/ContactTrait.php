<?php

namespace App\Http\Traits;

trait ContactTrait
{

    private function get_all_contacts()
    {
        return $this->contactModel::get();
    }

    private function store_contact($request)
    {
        return $this->contactModel::create($request->validated());
    }

    private function show_contact($id)
    {
        return $this->contactModel::find($id);
    }

    private function update_contact($request)
    {
        return $this->contactModel::find($request->id)->update($request->validated());
    }

    private function delete_contact($id)
    {
        return $this->contactModel::destroy($id);
    }
}
