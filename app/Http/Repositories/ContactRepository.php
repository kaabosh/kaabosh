<?php

namespace App\Http\Repositories;

use App\Http\Interfaces\ContactInterface;
use App\Http\Traits\apiResponse;
use App\Http\Traits\ContactTrait;
use App\Models\Contact;

class ContactRepository implements ContactInterface
{
    use ContactTrait, apiResponse;
    private $contactModel;

    public function __construct(Contact $contact)
    {
        $this->contactModel = $contact;
    }

    public function index()
    {
        $contacts =  $this->get_all_contacts();
        if ($contacts) {
            return $this->apiResponse(200, 'success', null, ['contacts' => $contacts]);
        }else{
            return $this->apiResponse(500, "Unexpected Error :(");
        }
    }

    public function store($request)
    {
        $contact = $this->store_contact($request);
        return $this->apiResponse(200, 'Data Inserted Successfully', null, $contact);
    }

    public function show($id)
    {
        $contact = $this->show_contact($id);
        if ($contact) {
            return $this->apiResponse(200, 'Data Is Found', null, ['contact' => $contact]);
        }else{
            return $this->apiResponse(404, "Data wasn't Found");
        }
    }

    public function update($request)
    {
        $contact = $this->update_contact($request);
        return $this->apiResponse(200,'Data Updated Successfully');
    }

    public function delete($request)
    {
        if ($this->delete_contact($request->id)) {
            return $this->apiResponse(200, 'Data Deleted Successfully');
        }else{
            return $this->apiResponse(500, 'Unexpected Error');
        }
    }
}
