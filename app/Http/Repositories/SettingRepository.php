<?php


namespace App\Http\Repositories;


use App\Http\Interfaces\SettingInterface;
use App\Http\Traits\apiResponse;
use App\Http\Traits\SettingTrait;
use App\Models\Setting;

class SettingRepository implements SettingInterface
{
    protected $settingsModel;
    use SettingTrait,
    apiResponse;
    function __construct(Setting $settings)
    {
        $this->settingsModel=$settings;
    }
    public function index()
    {
        $settings=$this->get_all_settings();
        if ($settings) {
            return $this->apiResponse(200, 'Success', null, ['settings'=>$settings]);
        }
        return $this->apiResponse(500, "Unexpected Error :(");
        
    }
    public function store($request)
    {
        $setting=$this->store_setting($request);
        return $this->apiResponse(200, "Data Stored Successfully :)",null,$setting);
    }
    public function show($id)
    {
        $setting=$this->get_setting($id);
        if ($setting) {
            return $this->apiResponse(200, 'Data was Found', null, ['setting'=>$setting]);
        }
        return $this->apiResponse(404, "No Records With This ID Was Found");

    }
    public function update($request)
    {
        $setting=$this->update_setting($request);
       return $this->apiResponse(201,'Data Updated Successfully :)',null,['setting'=>$setting]);
    }
    public function destroy($request)
    {
      if($this->destroy_setting($request->id)){
          return $this->apiResponse(203, "Data Deleted Successfully :)");
      }
      return $this->apiResponse(500, "Unexpected Error :(");

    }
}
