<?php


namespace App\Http\Repositories;


use App\Http\Interfaces\HomeInterface;
use App\Http\Traits\apiResponse;
use App\Http\Traits\HomeTrait;

class HomeRepository implements HomeInterface
{
    use HomeTrait,
    apiResponse;

    public function index()
    {
       return $this->apiResponse(200,'Welcome To Kaabosh');
    }
}
