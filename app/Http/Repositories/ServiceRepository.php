<?php


namespace App\Http\Repositories;


use App\Http\Interfaces\ServiceInterface;
use App\Http\Traits\apiResponse;
use App\Http\Traits\ServiceTrait;
use App\Models\Services;

class ServiceRepository implements ServiceInterface
{
    protected $servicesModel;
    use ServiceTrait,
    apiResponse;
    function __construct(Services $services)
    {
        $this->servicesModel=$services;
    }
    public function index()
    {
        $services=$this->get_all_services();
        if ($services) {
            return $this->apiResponse(200, 'Success', null, ['services'=>$services]);
        }
        return $this->apiResponse(500, "Unexpected Error :(");
        
    }
    public function store($request)
    {
        $service=$this->store_service($request);
        return $this->apiResponse(200, "Data Stored Successfully :)",null,$service);
    }
    public function show($id)
    {
        $service=$this->get_service($id);
        if ($service) {
            return $this->apiResponse(200, 'Data was Found', null, ['service'=>$service]);
        }
        return $this->apiResponse(404, "Data wasn't Found");

    }
    public function update($request)
    {
        $service=$this->update_service($request);
       return $this->apiResponse(201,'Data Updated Successfully :)',null,['service'=>$service]);
    }
    public function destroy($request)
    {
      if($this->destroy_service($request->id)){
          return $this->apiResponse(203, "Data Deleted Successfully :)");
      }
      return $this->apiResponse(500, "Unexpected Error :(");

    }
}
