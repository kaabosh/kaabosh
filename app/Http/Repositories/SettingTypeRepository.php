<?php


namespace App\Http\Repositories;


use App\Http\Interfaces\SettingTypeInterface;
use App\Http\Traits\apiResponse;
use App\Http\Traits\SettingTypeTrait;
use App\Models\SettingType;

class SettingTypeRepository implements SettingTypeInterface
{
    protected $settingTypesModel;
    use SettingTypeTrait,
    apiResponse;
    function __construct(SettingType $settingTypes)
    {
        $this->settingTypesModel=$settingTypes;
    }
    public function index()
    {
        $settingTypes=$this->get_all_settingTypes();
        if ($settingTypes) {
            return $this->apiResponse(200, 'Success', null, ['settingTypes'=>$settingTypes]);
        }
        return $this->apiResponse(500, "Unexpected Error :(");
        
    }
    public function store($request)
    {
        $settingType=$this->store_settingType($request);
        return $this->apiResponse(200, "Data Stored Successfully :)",null,$settingType);
    }
    public function show($id)
    {
        $settingType=$this->get_settingType($id);
        if ($settingType) {
            return $this->apiResponse(200, 'Data was Found', null, ['settingType'=>$settingType]);
        }
        return $this->apiResponse(404, "No Records With This ID Was Found");

    }
    public function update($request)
    {
        $settingType=$this->update_settingType($request);
       return $this->apiResponse(201,'Data Updated Successfully :)',null,['settingType'=>$settingType]);
    }
    public function destroy($request)
    {
      if($this->destroy_settingType($request->id)){
          return $this->apiResponse(203, "Data Deleted Successfully :)");
      }
      return $this->apiResponse(500, "Unexpected Error :(");

    }
}
