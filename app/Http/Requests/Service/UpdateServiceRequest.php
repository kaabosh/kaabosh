<?php

namespace App\Http\Requests\Service;

use Illuminate\Foundation\Http\FormRequest;

class UpdateServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'required|exists:services,id',
            'name'=>'required|string|min:5|unique:services,name',
            'icon'=>'required|string|min:3',
            'price_from'=>'required|numeric|gt:0',
            'price_to'=>'required|numeric|gt:price_from',
        ];
    }
}
