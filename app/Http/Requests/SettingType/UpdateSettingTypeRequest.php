<?php

namespace App\Http\Requests\SettingType;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSettingTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'required|exists:settings,id',
            'name'=>'required|string|min:3|unique:setting_types,name',
        ];
    }
}
