<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'required|exists:settings,id',
            'name'=>'required|string|min:3|unique:settings,name',
            'value'=>'required|string|min:3',
            'type_id'=>'required|exists:setting_types,id',
        ];
    }
}
