<?php

namespace App\Http\Requests\Contact;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Validate Data On Create
     * @return string[]
     */
    protected function onCreate(): array
    {
        return [
            'name' => 'required|regex:/^(?!.*\d)[a-z\p{Arabic}\s]+$/iu|min:3',
            'email' => 'required|email',
            'subject' => 'required|string|min:3',
            'description' => 'required|string|min:10',
        ];
    }

    protected function onUpdate(): array
    {
        return [
          'id' => 'required|exists:contacts,id',
          'status' => 'required|in:read,unread',
        ];
    }
    protected function onDelete(): array
    {
        return [
          'id' => 'required|exists:contacts,id',
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        if(request()->is('contacts/update'))
        {
            return $this->onUpdate();
        }else if(request()->is('contacts/delete'))
        {
            return $this->onDelete();
        }else{
            return $this->onCreate();
        }
    }
}
