<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Http\Interfaces\HomeInterface',
            'App\Http\Repositories\HomeRepository'
        );
        $this->app->bind(
            'App\Http\Interfaces\ServiceInterface',
            'App\Http\Repositories\ServiceRepository'
        );
        $this->app->bind(
            'App\Http\Interfaces\SettingInterface',
            'App\Http\Repositories\SettingRepository'
        );
        $this->app->bind(
            'App\Http\Interfaces\SettingTypeInterface',
            'App\Http\Repositories\SettingTypeRepository'
        );
        $this->app->bind(
            'App\Http\Interfaces\ContactInterface',
            'App\Http\Repositories\ContactRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
