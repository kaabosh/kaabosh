<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'value', 'type_id'];
    protected $hidden=['created_at','updated_at','type_id'];

    public function type(){
        return $this->belongsTo(SettingType::class,'type_id','id');
    }
}
